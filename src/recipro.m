function y=recipro(t)
%
%   Reciprosal of t
%

y = 1./t;

end
