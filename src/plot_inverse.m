function plot_inverse(s,t,x0,y0,ox,oy,theta,it,num,istop,steps)

fig1 = gcf;
f = fig1;
f.Position = [568 364 772 225];
     
    x = s.*cos(t);
    y = s.*sin(t);
    x = [x;x(1)];
    y = [y;y(1)];
    if num==1
        figure(fig1);
        subplot(1,3,1)
        plot(x,y,'r-',x0,y0,'b--',ox,oy,'k:','LineWidth',1.1)
        %title(['Newton method: ', int2str(num)],'FontSize',11)
        axis([-1 1 -0.8 1.4])
        set(gca,'TickLabelInterpreter','latex','FontSize',11)

        for l=1:size(theta,2)
            dir = arrow(theta(l),1);
            line(dir(:,1),dir(:,2),'LineWidth',1.1)
        end
        legend('\, reconstruction','\, initial guess','\, exact curve', 'interpreter','latex','Position',[0.161585196329182 0.692964501248086 0.181191979569467 0.233422198888991]);
    
    elseif (istop == 1) && (num < steps)
        figure(fig1);
        subplot(1,3,2)
        plot(x,y,'r-',x0,y0,'b--',ox,oy,'k:','LineWidth',1.1)
        %title(['Newton method: ', int2str(num)],'fontweight','bold','FontSize',11)
        axis([-1 1 -0.8 1.4])
        set(gca,'TickLabelInterpreter','latex','FontSize',11)
        
        for l=1:size(theta,2)
            dir = arrow(theta(l),1);
            line(dir(:,1),dir(:,2),'LineWidth',1.1)
        end
        legend('\, reconstruction','\, initial guess','\, exact curve', 'interpreter','latex','Position',[0.443283815492089 0.690563195075512 0.181191979569467 0.233422198888991]);
        pause(1);
    end
          
figure(fig1);
subplot(1,3,3)
plot((0:size(it,1)-1)', it(:,2),'-',(0:size(it,1)-1)', it(:,1),'--','LineWidth',1.1)
set(gca,'TickLabelInterpreter','latex','FontSize',11)
legend('\, residual error',['\, reconstruction' newline '\, error'], 'interpreter','latex','Position',[0.723659354744254 0.703671168904912 0.181191979569467 0.221101254721776]);


%print(gcf,'-depsc','0pernoise_1');

end