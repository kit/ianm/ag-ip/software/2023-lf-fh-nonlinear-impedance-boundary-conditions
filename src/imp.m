function [lambda,dlambda] = imp(t,r,dr)
%
%  impedance function 
%   

%% Example 1

lambda = 1./(ones(size(t,1),1)-0.2*sin(2*t));

% normal derivative
dtlambda = 0.4*cos(2*t)./((ones(size(t,1),1)-0.2*sin(2*t)).*(ones(size(t,1),1)-0.2*sin(2*t)));
dlambda = -dtlambda.*dr./(sqrt(r.*r+dr.*dr));

%% Example 2 and 3

%lambda = 1.8*ones(size(t,1),1) + cos(t).*cos(t).*cos(t) + sin(t).*sin(t).*sin(t);

%normal derivative
%dtlambda = 3*sin(t).*sin(t).*cos(t) - 3*cos(t).*cos(t).*sin(t);
%dlambda = -dtlambda.*dr./(sqrt(r.*r+dr.*dr));

end
