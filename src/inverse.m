%   Testing iterative Newton method for the inverse problem
%**************************************************

clear;

%  WAVE NUMBER
k  = 2;

% DIMENSION OF THE INVERSE PROBLEM
n = 64;

% INCIDENT DIRECTIONS
theta = pi/6;

% ARTIFICAL DATA
n_orig   = 2*n;
[r_orig, dr_orig, ddr_orig, dddr_orig] = bndry(n_orig,1);

r = input('Generate data? y/n [y]','s');
if isempty(r)
   r = 'y';
end
if r=='y'
    [ui,dui] = inwave(k,theta,r_orig,dr_orig);
    t_orig = 2*pi*(0:n_orig-1)'/ n_orig;
    [lambda,dlambda] = imp(t_orig,r_orig,dr_orig);
   
    g = @g;
    gz = @gz;
    u_n = ui;
    for m=1:10
        [A,F_A,AG,K,F_AG,S,L,phi_A,phi_AG] = M_TM(n_orig,k,r_orig,dr_orig,ddr_orig,u_n,theta);
        rhs3 = S*(gz(u_n,u_n)-g(u_n))+ui;
        rhs3 = [real(rhs3); imag(rhs3)];
        u_n_old = u_n;
        u_n = phi_AG\(rhs3);
        u_n = u_n(1:size(u_n_old,1),:) + 1i*u_n((size(u_n_old,1)+1):size(u_n,1),:);
    end

    ug  = u_n;
    dug = -1i*k*lambda.*ug + gz(u_n_old,ug) + g(u_n_old) - gz(u_n_old,u_n_old);
      
    ff  = F_AG*[ug;-dug];   
  
    data0 = select(ff,2);

% NOISE
    data = noisec(data0,0);

    save last data;
else
    r = input('Which file ?', 's')
    load(r)
end


%%% INVERSE PROBLEM %%%


%  STARTING CURVE
s0 = 0.3*ones(n,1);  % Example 1
%s0 = 0.6*ones(n,1); % Example 2 and 3
   
%% Newton method
    % PARAMETER
    Nmod     = 4;
    a        = 0.05;
    taudelta = 0.0011;
    steps    = 21;

    [it1,s1] = new_TM(k,theta,data,Nmod,a,taudelta,steps,s0,r_orig,2);