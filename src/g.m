function y = g(u)

%% Example 1
y = u + sin(u);           % apple and peanut

%% Example 2
%y = (abs(u).^2).*u;      % garlic

%% Example 3
%on2 = ones(size(u));
%y = u./(on2+abs(u).^2);  % garlic
end

