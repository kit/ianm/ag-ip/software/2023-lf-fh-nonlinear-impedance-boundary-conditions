function [u,du] = inwave(k,theta,r,dr,tcos,tsin)
%
%   Incident wave and its derivative on a starlike boundary $r$
%
%   INPUT:    k           wave number 
%             theta       angles of incidence
%             r           boundary     
%             dr          derivative of r          (otional)
%             tcos, tsin  unit sphere, size of r   (otional)
%
%   OUTPUT:   u           Dirichlet values of the incident fields
%             du          Neumann values of the incident fields
%
%   Syntax:   [u,du] = inwave(k,theta,r,dr,tcos,tsin)
%******************************************************************

d1 = cos(theta);
d2 = sin(theta);

if nargin < 4
   dr = dtrig(r,0);
else
end

if nargin < 5
   n    = size(r,1) / 2;
   t    = pi/n * (0:2*n-1)';
   tcos = cos(t);
   tsin = sin(t);
else
end

u       = exp(1i*k*((r.*tcos)*d1+(r.*tsin)*d2));
abs     = sqrt(r.^2+dr.^2);
normal1 = (dr.*tsin + r.*tcos) ./ abs; 
normal2 = -(dr.*tcos - r.*tsin) ./ abs;
du      = 1i*k*(normal1*d1 +  normal2*d2).*u;

end