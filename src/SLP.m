function [M,gew,sln,bessel,hankel]=SLP(k,r,dr,gew,sln,bessel,hankel)
%
%  matrix of the single layer potential 
%
%  INPUT:  k       wave number 
%          r       starlike parametrization of the boundary
%        optional:
%          dr      derivative of r
%          gew     weight of the quadratur formula (vector |i-j|)
%          sln     ln(4 sin^2((t_i-t_j)/2)         (vector |i-j|)
%          bessel  J0(k|x-y|)
%          hankel  Y0(k|x-y|)
%
%  OUTPUT: M       matrix
%          gew     weight of the quadratur formula (vector |i-j|)
%          sln     ln(4 sin^2((t_i-t_j)/2)         (vector |i-j|)
%          bessel  J0(k|x-y|)
%          hankel  Y0(k|x-y|)
%
%  SYNTAX: [M,gew,sln,bessel,hankel]=slp(k,r,dr,gew,sln,bessel,hankel)
%*******************************************************************

euler = 0.57721566490153286061;
n     = size(r,1) / 2;

% derivatives
if nargin < 3
   dr = dtrig(r);
else
end

% preparing the quadratur formula
if nargin <4 
   mintg = (1:n-1)';
   mcos = cos( pi/n * (0:2*n-1)' * mintg');
   gew = -2*pi/n*mcos*(ones(n-1,1)./mintg);
   gew = gew - pi/(n^2)*(-ones(2*n,1)).^((0:2*n-1)');

   sln = log(4*(sin( (1:2*n-1)' *pi / (2*n) )).^2);
   sln = [0;sln];

   Rij = toeplitz(gew);
   Sij = toeplitz(sln);
else
   Rij = toeplitz(gew);
   Sij = toeplitz(sln);
end

%  k * |x(t_i) - x(t_j)|
t     = pi/n * (0:2*n-1)';
tcos  = cos(t);
tsin  = sin(t);
x1    = r.*tcos;     % x_1(t_i)
x2    = r.*tsin;     % x_2(t_i)
nones = ones(size(r));
X1    = x1*nones';
X2    = x2*nones';
Diff  = k*sqrt((X1-X1').^2 + (X2-X2').^2);  

% norm of the tangential vector
absdx =  sqrt(r.^2 + dr.^2);

% bessel functions
if nargin < 6
   bessel   = full(spfun('j0', triu(Diff,1)));
   hankel   = full(spfun('y0', triu(Diff,1)));
   bessel   = bessel + bessel' + diag(ones(2*n,1));
   hankel   = triu(bessel,1) + tril(bessel,-1) + 1i*(hankel + hankel');
end

% matrix
M1  = -(ones(2*n,1)*absdx').*bessel / (4*pi);
M2  = 1i/4*hankel.*(ones(2*n,1)*absdx');
tmp = ( (1i/4-euler/2/pi)*ones(2*n,1)-log(k*absdx/2)/2/pi ).*absdx;
M2  = M2 + diag(tmp); 
M2  = M2 - M1.*Sij;

M = Rij.*M1 + pi/n * M2;

end





