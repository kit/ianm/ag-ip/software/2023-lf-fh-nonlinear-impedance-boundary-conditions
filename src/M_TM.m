function [A,F_A,AG,K,F_AG,S,L,phi_A,phi_AG]=M_TM(m,k,r,dr,ddr,u,theta)
% **********************************************
%   *    Matrices for the problem   *
% **********************************************
%   Operator solving the problem by using a combined ansatz or the representation theorem
%          ----------------------
%
%   INPUT:
%            m              size of the far field pattern
%            k,             wave numbers
%            r              starlike parametrization of the boundary
%            (dr, ddr,dddr) derivatives of r 
%            theta          angles of incidence
%
%   OUTPUT:
%            A   operator                 ( phi = A\ [f;g] )
%            F   far field operator       ( ff  = F*phi   )
%            AG  operator using the representation theorem
%                                         ( AG*(u;du) = (ui;dui) )
%                corresponding far field by F*[ v; -nd(v) ] 
%
%   Syntax:  [A,AG,F] = M_TM(m,k,r,dr,ddr, u, theta)

%*****************************************************************

% INTEGRAL EQUATION

[K,gew,sln,b1,h1] = DLP(k,r,dr,ddr);
[L,gew,sln,b1,h1] = NDSLP(k,r,dr,ddr,gew,sln,b1,h1);
[S,gew,sln,b0,h0] = SLP(k,r,dr,gew,sln);

E = eye(size(S));

s     = 2*pi*(0:m-1)' / m;
[lambda,dlambda] = imp(s,r,dr);

H1 = ones(size(lambda))'.*lambda;
gz = @gz;

for j=1:size(theta,2)
    on = ones(size(u(:,j)));
    uh = u;      % Example 1
    %uh = -1i*u; % Example 2
    %uh = 1i*u;  % Example 3
    H2 = ones(size(gz(u(:,j),on)))'.*gz(u(:,j),on);
    H3 = ones(size(gz(u(:,j),on)))'.*gz(uh(:,j),on);

    A = -0.5*E+L+1i*k*H1.*S-H2.*S;
    A1 = -0.5*E + real(L) - k*real(H1).*imag(S) - k*imag(H1).*real(S) - real(H2).*real(S) + imag(H3).*imag(S);
    A2 = -imag(L) - k*real(H1).*real(S) + k*imag(H1).*imag(S) + real(H2).*imag(S) + imag(H3).*real(S);
    A3 = imag(L) + k*real(H1).*real(S) - k*imag(H1).*imag(S) - imag(H2).*real(S) - real(H3).*imag(S);
    A4 = -0.5*E + real(L) - k*real(H1).*imag(S) - k*imag(H1).*real(S) + imag(H2).*imag(S) - real(H3).*real(S);
    phi_A = [A1 A2; A3 A4];

    AG = 0.5*E-K-S.*(1i*k*(H1.'))+S.*(H2.');
    AG1 = 0.5*E - real(K) + imag(S).*(k*real((H1.'))) + real(S).*(k*imag((H1.'))) + real(S).*real((H2.')) - imag(S).*imag((H2.'));
    AG2 = imag(K) + real(S).*(k*real((H1.'))) - imag(S).*(k*imag((H1.')))-imag(S).*real((H3.')) - real(S).*imag((H3.'));
    AG3 = -imag(K) - real(S).*(k*real((H1.'))) + imag(S).*(k*imag((H1.')))+imag(S).*real((H2.')) + real(S).*imag((H2.'));
    AG4 = 0.5*E - real(K) + imag(S).*(k*real((H1.'))) + real(S).*(k*imag((H1.'))) + real(S).*real((H3.')) - imag(S).*imag((H3.'));
    phi_AG = [AG1 AG2; AG3 AG4];
end

% FAR FIELD OPERATOR
n     = size(r,1);
const = exp(1i*pi/4)/sqrt(8*pi*k) * 2*pi/n;
s     = 2*pi*(0:m-1)' / m;
scos  = cos(s);
ssin  = sin(s);

t    = 2*pi/n * (0:n-1)';
tcos = cos(t);
tsin = sin(t);

x1      = r.*tcos;
x2      = r.*tsin;
absdx   = sqrt(r.^2+dr.^2);
normal1 = (r.*tcos + dr.*tsin)./absdx;
normal2 = (r.*tsin - dr.*tcos)./absdx;

F2 = exp(-1i*k*(scos*x1'+ ssin*x2')) .* (ones(size(scos))*(absdx)');
F1 = -1i*k*(scos*normal1' + ssin*normal2');
F1 = F1 .* F2;

F_A  = const*F2; 
F_AG = const * [F1,F2];

end
