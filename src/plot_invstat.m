function plot_invstat(s,t,x0,y0,ox,oy,theta,it,num,istop,steps)

x = s.*cos(t);
y = s.*sin(t);
x = [x;x(1)];
y = [y;y(1)];
    if num==1
        subplot(1,3,1)
        plot(x,y,'r-',x0,y0,'b--',ox,oy,'k:','LineWidth',1.1)
        title(['Newton method: ', int2str(num)],'FontSize',11,'interpreter','latex')
        axis([-1.2 1.2 -0.8 1.49])    % Example 1, peanut
        %axis([-1.8, 1.8, -1.3, 2.3]) % Example 2 and 3, garlic
        set(gca,'TickLabelInterpreter','latex','FontSize',11)

        for l=1:size(theta,2)
            dir = arrow(theta(l),1.2);   % Example 1, peanut
            %dir = arrow(theta(l),1.9);  % Example 2 and 3, garlic
            line(dir(:,1),dir(:,2),'LineWidth',1.1)
        end
        legend('\, reconstruction','\, initial guess','\, exact curve', 'interpreter','latex','Position',[0.189368719527231 0.667791919510702 0.154222941816569 0.231365615638868]);  % Example 1, peanut
        %legend('\, reconstruction','\, initial guess','\, exact curve','interpreter','latex','Position',[0.174778124247324 0.679990155302052 0.167722072215382 0.222542350635691]);  % Example 2 and 3, garlic

    elseif (istop == 1) && (num < steps)
        subplot(1,3,2)
        plot(x,y,'r-',x0,y0,'b--',ox,oy,'k:','LineWidth',1.1)
        title(['Newton method: ', int2str(num)],'fontweight','bold','FontSize',11,'interpreter','latex')
        axis([-1.2 1.2 -0.8 1.49])    % Example 1, peanut
        %axis([-1.8, 1.8, -1.3, 2.3]) % Example 2 and 3, garlic
        set(gca,'TickLabelInterpreter','latex','FontSize',11)
        for l=1:size(theta,2)
            dir = arrow(theta(l),1.2);  % Example 1, peanut
            %dir = arrow(theta(l),1.9); % Example 2 and 3, garlic
            line(dir(:,1),dir(:,2),'LineWidth',1.1)
        end
        legend('\, reconstruction','\, initial guess','\, exact curve', 'interpreter','latex','Position',[0.46996965450055 0.669718951448664 0.154222941816569 0.231365615638868]);   % Example 1, peanut
        %legend('\, reconstruction','\, initial guess','\, exact curve', 'interpreter','latex','Position',[0.456416403618984 0.681917187240013 0.167722072215381 0.222542350635691]); % Example 2 and 3, garlic
        pause(1);
    end
      
subplot(1,3,3)
plot((0:size(it,1)-1)', it(:,2),'-',(0:size(it,1)-1)', it(:,1),'--','LineWidth',1.1)
set(gca,'TickLabelInterpreter','latex','FontSize',11)
legend('\, residual error',['\, reconstruction' newline '\, error'], 'interpreter','latex','Position',[0.751445303763716 0.678405835763909 0.154222941816569 0.219153226045814]);  % Example 1, peanut
%legend('\, residual error',['\, reconstruction' newline '\, error'], 'interpreter','latex','Position',[0.737016404852854 0.68982395695915 0.167722072215382 0.210795687764405]);  % Example 2 and 3, garlic

end