function [itl,sl] = new_TM(k,theta,g1,Nmod,a1,taudelta,steps,s0,r_orig,plot)
%
%  Newton method solving the inverse problem
%   
%  INPUT:   k       wave numbers
%           theta   angles of incidence
%           g       measured far field patterns  ( even dim ! )
%           Nmod    Degree of the trig approx of the boundary curve
%           a1      regularisation parameter
%           steps   upper bound of iteration steps
%           s0      starting curve  (optional, default s0=1.0)
%           r_orig  original object (optional, if non -> no plot)
%
%  OUTPUT:  num    number of iterations
%           s      solution curve
%
%  SYNTAX:  [num,s] = new_TM(k,theta,g,Nmod,a1,taudelta,steps,s0,r_orig,plot)
%********************************************************************

n2 = (size(g1,1));
n  = n2/2;

% starting curve
s          = s0;
[ds,Dtrig] = dtrig(s);

% initial guess
ds       = Dtrig* s;
[ui,ndui] = inwave(k,theta,s,ds);
u_n = ui;

% prepare plots
t  = pi / n * (0:n2-1)';
x0 = s0.*cos(t);
y0 = s0.*sin(t);
x0 = [x0;x0(1)];
y0 = [y0;y0(1)];

n_orig = size(r_orig,1);
t_orig = 2*pi*(0:n_orig-1)'/ n_orig;
ux    = cos(t_orig);
uy    = sin(t_orig);
ox    = r_orig.*ux;
oy    = r_orig.*uy;


% trig. polynomials
B = t * (1:Nmod);
B = [ ones(size(t)), cos(B), sin(B) ];

datanorm = (sqrt(pi* diag(g1'*g1) / n))';
stopping = 10^(-7);
num      = 0;
rsold    = 1;
rs       = 0; 
damp     = 1;
istop    = 1;

%  ITERATION STEP
while (norm(rs-rsold) > stopping) && (num < steps) 

      ds       = Dtrig* s;
      dds      = Dtrig*ds;
     
      [ui,ndui] = inwave(k,theta,s,ds);
      [lambda,dlambda] = imp(t,s,ds);
      
%% DIRECT PROBLEM:
      g = @g;
      gz = @gz;

      [A,F_A,AG,K,F_AG,S,L,phi_A,phi_AG] = M_TM(n2,k,s,ds,dds,u_n,theta);
      rhs4 = -ndui-1i*k*lambda.*ui + gz(u_n,ui) + g(u_n) - gz(u_n,u_n);
      rhs4 = [real(rhs4); imag(rhs4)];
      u_n_old = u_n;
      phi_n = phi_A\(rhs4);
      phi_n = phi_n(1:size(u_n_old,1),:) + 1i*phi_n((size(u_n_old,1)+1):size(phi_n,1),:);
      u_n = S*phi_n + ui;
      
      ug = u_n;
      dug = -1i*k*lambda.*ug + gz(u_n_old,ug) + g(u_n_old) - gz(u_n_old,u_n_old);
  
      ff       = F_A*phi_n;
      rshelp = sum(sqrt(pi*(diag((g1-ff)'*(g1-ff)))/n) ./ datanorm') / size(theta,2);

%% DAMPING OR STOP IF INCREASING
      if (rshelp > rs) && (num > 0)
%      if num < 0 
         if (damp > 4)
            subplot(1,3,3)
            plot((0:size(it,1)-1)', it(:,2),'-',(0:size(it,1)-1)', it(:,1),'--') 
            return
         else
            s    = s - 1/(2^damp)*B*h;
            damp = damp + 1;
         end
      else
         num   = num + 1;
         rsold = rs;
         rs    = rshelp;

%% TEST
         if (num == 1)
            it = [ norm(pi/n*(s-select(r_orig,2))), rs];
            itl = it;
            sl  = s; 
         else
            it = [it; [ norm(pi/n*(s-select(r_orig,2))), rs]];
            if istop ==1
               itl = it;
               sl  = s; 
            end
         end
%%
         if rs < taudelta
            istop = 2;
         end
         damp  = 1;
         tmp   = zeros(size(s));
         [lambda,dlambda] = imp(t,s,ds);
         kappa = 2*ds.^2-s.*dds+s.^2./(ds.^2+s.^2)./sqrt(ds.^2+s.^2);
         nufak = s./(s.^2+ds.^2);
         R     = (1:Nmod).^2;
         R     = [0;R';R'];
         R     = eye(size(B,2)) + diag(R);
         for j=1:size(theta,2)
%% NEWTON STEP
         tmp1h = nufak.*(Dtrig*ug(:,j))./(ds.^2+s.^2).^(1/2);
         tmp1 = Dtrig * ( B.* (tmp1h  * ones(1,size(B,2))) ) ;
         b1   = tmp1./( (ds.^2+s.^2).^(1/2) * ones(1,size(B,2)) ); 
         b2  = B.* ( k^2*(nufak.*ug(:,j) * ones(1,size(B,2))) );
         b3  = B.* ( -2*1i*k*(kappa.*lambda.*nufak.*ug(:,j) * ones(1,size(B,2))));
         b4  = B.* ( -1i*k*(lambda.*nufak.*dug(:,j) * ones(1,size(B,2))));
         b5  = B.* ( -1i*k*(dlambda.*nufak.*ug(:,j) * ones(1,size(B,2))));
         b10 = B.* (  gz(ug(:,j),(nufak.*dug(:,j))) * ones(1,size(B,2)) );
         b11 = B.* ( 2*kappa.*g(ug(:,j)).*nufak* ones(1,size(B,2)) );
         b = b1+b2+b3+b4+b5+b10+b11;
         b = [real(b); imag(b)];
         phi2 = phi_A\b;
         phi2 = phi2(1:size(ug,1),:) + 1i*phi2((size(ug,1)+1):size(phi2,1),:);
         J    = F_A*phi2;
         

         h1   = (J'*J + a1 * R ) \ (J'*(g1(:,j)-ff(:,j)));
         h    = real(h1);
        
%% UPDATE
         tmp  = tmp + B*h;
         end
      s = s + tmp / size(theta,2);
      end
      %% PLOT
      if plot == 1
          plot_invstat(s,t,x0,y0,ox,oy,theta,it,num,istop,steps)
      else
          plot_inverse(s,t,x0,y0,ox,oy,theta,it,num,istop,steps)
      end
      
end

end
