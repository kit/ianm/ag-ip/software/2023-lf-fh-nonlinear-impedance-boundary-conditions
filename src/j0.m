function y=j0(t)
%   Bessel function j_0(t)

y = besselj(0,t);

end