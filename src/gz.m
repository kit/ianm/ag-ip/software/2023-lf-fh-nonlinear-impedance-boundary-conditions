function [v] = gz(u,z)

%% Example 1                                          % apple and peanut
v = (1 + cos(u)).*z;                                  % M_TM    --> u_h = u
                                                      % invstat --> a = 2
                                                      % inverse --> s0 = 0.3*ones(n,1)

%% Example 2                                          % garlic
%v = (abs(u).^2).*z + 2*real(conj(u).*z).*u;          % M_TM    --> u_h = -iu
                                                      % invstat --> a = 1.8

%% Example 3                                          % garlic
%on1 = ones(size(u));
%v_h = (on1 + abs(u).^2).*z - 2*real(u.*conj(z)).*u;
%v = v_h./((on1 + abs(u).^2).^2);                     % M_TM    --> uh = iu
                                                      % invstat --> a = 1.8

end

