function a = arrow(theta,L)
%
%   creates an arrow by use of 'line'
%       theta  angle            
%       L      size

%*****************************************************************

if ((theta > -pi/4) && (theta <= pi/4))
   x1 = -L;
   x2 = - L*sin(theta) / cos(theta);
elseif ((theta > pi/4) && (theta <= 3*pi/4))
   x2 = -L;
   x1 = - L*cos(theta) / sin(theta);
elseif ((theta > 3*pi/4) && (theta <= 5*pi/4))
   x1 = L;
   x2 = L*sin(theta) / cos(theta);
else
   x2 = L;
   x1 = L*cos(theta) / sin(theta);
end

a  = [x1,x2];
b1 = x1 + L*0.3*cos(theta);
b2 = x2 + L*0.3*sin(theta);
a  = [a;[b1,b2]];
c1 = b1 + L*0.1*cos(theta+7*pi/8);
c2 = b2 + L*0.1*sin(theta+7*pi/8);
a  = [a;[c1,c2];[b1, b2]];
c1 = b1 + L*0.1*cos(theta-7*pi/8);
c2 = b2 + L*0.1*sin(theta-7*pi/8);
a  = [a;[c1,c2]];
