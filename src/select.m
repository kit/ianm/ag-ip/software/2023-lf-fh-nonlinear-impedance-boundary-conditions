function y = select(v,nth)
%
%   Selecting 1/nth values from v
%
%   Syntax: select(v,nth)

%*************************************************
M = size(v,1);

if rem(M,nth) == 0
   dim = M/nth;
   A   = zeros(dim,M);   
   for l=1:dim
      A(l,1+(l-1)*nth) = 1;
   end
   y = A*v;
else
   'no selection'
   return;
end

end
