function [y,Dtrig] = dtrig(s)
%
%  dtrig:  approximation of the derivative of a 2pi-periodic 
%          function by trigonometric interpolation on [0,2 pi]
%   
%  INPUT:  
%          s  values at the interpolation points, x_i= i*pi / n
%             (dimension: 2n)
% 
%  OUTPUT: y       d/dt P_n (t_i)
%          Dtrig   matrix with A h = h' at interpolation points
%                  (optional)
%**********************************************************************

n     = size(s,1) / 2;

intg  = (1:2*n-1)';
theta = pi*intg / n;           % interpolation points  (no t_0=0)

aj    = cos(n*theta).*cot(theta / 2) - sin(n*theta)./ (2*n*((sin (theta/2)).^2));
aj    = aj / 2;
aj    = [0;aj];
Dtrig = toeplitz(aj,-aj);

y     = Dtrig*s;

end