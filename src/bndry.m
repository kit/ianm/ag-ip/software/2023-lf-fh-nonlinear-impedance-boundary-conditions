function [r,dr,ddr,dddr]=bndry(n,geom,a,b,c)
%
%   *  STARLIKE BOUNDARIES   *
%
%  INPUT:  n     dimension 2n of the discretization
%          geom  geometry
%                1: cardiod
%                    a  size
%                    b  size
%                2: peanut 
%
%  OUTPUT:   r   parametrization
%           dr   derivative
%          ddr   second derivative
%         dddr   third derivative
%
%  Syntax: bndry(n,geom,a,b,c)
%
%*****************************************************************

t    = 2*pi*(0:n-1)' / n;

if geom == 1
   % cardiod
  
   %% default
   if nargin < 5
      c=0.7;
      if nargin < 4
         b = 0.1;
         if nargin < 3
            a = 0.4;
         end
      end
   end
    
   r   = 0.5*ones(n,1) + a * cos(t) + b * sin(2*t);
   r   = r./ ((ones(n,1) + c*cos(t))) ;
   dr  = dtrig(r);
   ddr = dtrig(dr);
   dddr = dtrig(ddr);

   elseif geom == 2
   % peanut
  
   r = cos(t).*cos(t) + 0.25*sin(t).*sin(t); 
   r   = sqrt(r);
   dr  = dtrig(r);
   ddr = dtrig(dr);
   dddr = dtrig(ddr);

   elseif geom == 3
   % garlic
  
   r = ones(n,1)-sin(t).*cos(t).*cos(t); 
   dr  = dtrig(r);
   ddr = dtrig(dr);
   dddr = dtrig(ddr);

end








