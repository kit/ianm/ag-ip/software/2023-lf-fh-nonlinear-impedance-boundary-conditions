function y = noisec(x,p)
%
%   Adding p%  random noise to the columns of x (! complex valued !)
%   (% wrt L2 norm of column in [0,2pi])
%
%   Syntax: noise(x,p)
%***************************************************

y      = rand(size(x));
phi    = 2*pi*rand(size(x));
l2norm = sqrt(2*pi/(size(x,1))*diag(x'*x));
y      = x + (ones(size(x,1),1)*l2norm').*y.*exp(i*phi) * p*0.01;

end