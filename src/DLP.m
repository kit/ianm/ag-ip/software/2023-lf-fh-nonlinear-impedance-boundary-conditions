function [L,gew,sln,bessel,hankel]=DLP(k,r,dr,ddr,gew,sln,bessel,hankel)
%
%  matrix of the double layer potential 
%
%  INPUT:  k       wave number 
%          r       starlike parametrization of the boundary
%        optional:
%          dr, ddr derivatives of r
%          gew     weight of the quadratur formula (vector |i-j|)
%          sln     ln(4 sin^2((t_i-t_j)/2)         (vector |i-j|)
%          bessel  J1(k|x-y|)
%          hankel  Y1(k|x-y|)
%
%  OUTPUT: L       matrix
%          gew     weight of the quadratur formula (vector |i-j|)
%          sln     ln(4 sin^2((t_i-t_j)/2)         (vector |i-j|)
%          bessel  J1(k|x-y|)
%          hankel  Y1(k|x-y|)
%
%  SYNTAX: [L,gew,sln,bessel,hankel]=DLP(k,r,dr,ddr,gew,sln,bessel,hankel)
%***************************************************************

n = size(r,1) / 2;

% DERIVATIVES
if nargin < 3
   dr  = dtrig(r);
   ddr = dtrig(dr);
else
end

% PREPARING THE QUADRATUR
if nargin <5
   mintg = (1:n-1)';
   mcos  = cos( pi/n * (0:2*n-1)' * mintg' );
   gew   = -2*pi/n * mcos*( ones(n-1,1)./mintg );
   gew   = gew - pi/(n^2)*( -ones(2*n,1)).^((0:2*n-1)') ;
   sln   = log( 4*( sin( (1:2*n-1)'*pi/(2*n) )).^2  );
   sln   = [0;sln];
   Rij   = toeplitz(gew);
   Sij   = toeplitz(sln);
else
   Rij   = toeplitz(gew);
   Sij   = toeplitz(sln);
end

%  |x(t_i) - x(t_j)|
t     = pi/n * (0:2*n-1)';
tcos  = cos(t);
tsin  = sin(t);
x1    = r.*tcos;
x2    = r.*tsin;
dx1   = dr.*tcos - r.*tsin;
dx2   = dr.*tsin + r.*tcos;
nones = ones(size(r));
X1    = x1*nones';
X2    = x2*nones';
Diff  = sqrt((X1-X1').^2 + (X2-X2').^2);  

% BESSEL FUNCTIONS
if nargin < 7
   bessel   = full(spfun('j1', triu(k*Diff,1)));
   hankel   = full(spfun('y1', triu(k*Diff,1)));
   bessel   = bessel + bessel';
   hankel   = bessel + 1i*(hankel + hankel');
end 

% MATRIX
Diff = triu(Diff,1) + tril(Diff,-1) + diag(ones(size(r)));
L2   = 1i*k/4*(hankel)./Diff;
tmp  = (X1-X1').*(nones * dx2') - (X2-X2').*(nones * dx1');
L1   = -k/4/pi*(bessel./Diff).*tmp;
L2   = L2.*tmp;
tmp  = -1/4/pi * (2*dr.^2 - r.*ddr + r.^2) ./ (r.^2 + dr.^2); 
L2   = L2 + diag(tmp); 
L2   = L2 - L1.*Sij;

L    = Rij.*L1 + pi/n * L2;



