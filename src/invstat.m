%
%   The inverse problem
%    
%**************************************************

clear;

%  WAVE NUMBER
k  = 2;

% DIMENSION OF THE USED DATA (FAR FIELD PATTERN)
n = 64;

% INCIDENT DIRECTIONS
theta = pi/6;

n_orig = 2*n;
[r_orig, dr_orig, ddr_orig, dddr_orig] = bndry(n_orig,2);  % Example 1, peanut
%[r_orig, dr_orig, ddr_orig, dddr_orig] = bndry(n_orig,3); % Example 2, garlic


% ARTIFICAL DATA
    [ui,dui] = inwave(k,theta,r_orig,dr_orig);
    t_orig = 2*pi*(0:n_orig-1)'/ n_orig;
    [lambda,dlambda] = imp(t_orig,r_orig,dr_orig);

    g = @g;
    gz = @gz;     
    u_n = ui;
    for m=1:10
        [A,F_A,AG,K,F_AG,S,L,phi_A,phi_AG] = M_TM(n_orig,k,r_orig,dr_orig,ddr_orig,u_n,theta);
        rhs3 = S*(gz(u_n,u_n)-g(u_n))+ui;
        rhs3 = [real(rhs3); imag(rhs3)];
        u_n_old = u_n;
        u_n = phi_AG\(rhs3);
        u_n = u_n(1:size(u_n_old,1),:) + 1i*u_n((size(u_n_old,1)+1):size(u_n,1),:);
    end

    ug  = u_n;
    dug = -1i*k*lambda.*ug + gz(u_n_old,ug) + g(u_n_old)-gz(u_n_old,u_n_old);
      
    ff  = F_AG*[ug;-dug];

    data0 = select(ff,2);

%  STARTING CURVE
s0 = 0.6*ones(n,1); 
   
% PARAMETER 
delta = 0.065;
steps = 13;
Nmod  = 4;
a     = 2;    % Example 1, peanut
%a     = 1.8; % Example 2 and 3, garlic

% prepare plots
t     = 2*pi / n * (0:n-1)';
t_orig = 2*pi*(0:n_orig-1)'/ n_orig;
ux    = cos(t_orig);
uy    = sin(t_orig);
ox    = r_orig.*ux;
oy    = r_orig.*uy;

x0    = s0.*cos(t);
y0    = s0.*sin(t);
x0    = [x0;x0(1)];
y0    = [y0;y0(1)];

pause off

for itest = 1:100
    data = noisec(data0,5);
    if itest==1
        stat = data;
    else
        stat = [stat, data];
    end

    [it1,s1] = new_TM(k,theta,data,Nmod,a,delta,steps,s0,r_orig,1);

    if itest == 1
        error1 = norm(pi/n*(s1-select(r_orig,2)));
    else
        error1 = [error1; norm(pi/n*(s1-select(r_orig,2)))];
    end
end

v1 = mean(error1);
v1 = v1*ones(size(error1));
[v1,mean1] = min(abs(v1-error1)) ;

[v1,max1] = max(error1);
[v1,min1] = min(error1);

pause on

fig1 = gcf;
f = fig1;
f.Position = [542 208 595 234];  % Example 1, peanut
%f.Position = [465 293 662 271]; % Example 2 and 3, garlic

fac = size(theta,2);
sub = fac - 1;

figure('Position',[337 275 909 229])  % Example 1, peanut
%figure('Position',[401 397 834 236]) % Example 2 and 3, garlic
data = stat(:,fac*min1-sub:fac*min1);
[it1,s1] = new_TM(k,theta,data,Nmod,a,delta,steps,s0,r_orig,1);

figure(fig1);
    it1 = (size(it1,1))-1;
    x   = s1.*cos(t);
    y   = s1.*sin(t);
    x   = [x;x(1)];
    y   = [y;y(1)];
    subplot(1,2,1)
        plot(x,y,'r-',0.6*ux,0.6*uy,'b--',ox,oy,'k:','LineWidth',1.1)
        %title(['Newton (min): ', num2str(it2)])
        axis([-1.2, 1.2, -0.8, 1.49]) % Example 1, peanut
        %axis([-1.8, 1.8, -1.3, 2.3]) % Example 2 and 3, garlic
        set(gca,'TickLabelInterpreter','latex','FontSize',11)
    for l=1:size(theta,2)
        dir = arrow(theta(l),1.2);  % Example 1, peanut
        %dir = arrow(theta(l),1.9); % Example 2 and 3, garlic
        line(dir(:,1),dir(:,2),'LineWidth',1.1)
    end
    legend('\, reconstruction','\, initial guess','\, exact curve', 'interpreter','latex','Position',[0.22823535279091 0.697856345183539 0.235092786937191 0.224444422008646]);   % Example 1, peanut
    %legend('\, reconstruction','\, initial guess','\, exact curve', 'interpreter','latex','Position',[0.240381164283714 0.715825603105372 0.224875633299367 0.209741704595924]); % Example 2 and 3, garlic

figure('Position',[374 250 907 227])  % Example 1, peanut
%figure('Position',[421 369 833 237]) % Example 2 and 3, garlic
data = stat(:,fac*max1-sub:fac*max1);
[it1,s1] = new_TM(k,theta,data,Nmod,a,delta,steps,s0,r_orig,1);

figure(fig1);
    it1 = (size(it1,1))-1;
    x   = s1.*cos(t);
    y   = s1.*sin(t);
    x   = [x;x(1)];
    y   = [y;y(1)];
    subplot(1,2,2)
        plot(x,y,'r-',0.6*ux,0.6*uy,'b--',ox,oy,'k:','LineWidth',1.1)
        %title(['Newton (max): ', num2str(it2)])
        axis([-1.2, 1.2, -0.8, 1.49]) % Example 1, peanut
        %axis([-1.8, 1.8, -1.3, 2.3]) % Example 2 and 3, garlic
        set(gca,'TickLabelInterpreter','latex','FontSize',11)
    for l=1:size(theta,2)
        dir = arrow(theta(l),1.2);  % Example 1, peanut
        %dir = arrow(theta(l),1.9); % Example 2 and 3, garlic
        line(dir(:,1),dir(:,2),'LineWidth',1.1)
    end
    legend('\, reconstruction','\, initial guess','\, exact curve', 'interpreter','latex','Position',[0.668748460980656 0.697856345183539 0.23509278693719 0.224444422008646]);  % Example 1, peanut
    %legend('\, reconstruction','\, initial guess','\, exact curve', 'interpreter','latex','Position',[0.677727001493946 0.714856103430767 0.22710683536406 0.209741704595924]); % Example 2 and 3, garlic


%exportgraphics(fig1,'5pernoise_2.eps','BackgroundColor','none')    


 