function [L,gew,sln,bessel1,hankel1]=NDSLP(k,r,dr,ddr,gew,sln,bessel1,hankel1)
%
%  Matrix for the normal derivative of the single layer potential 
%  
%
%  INPUT:  k        wave number 
%          r,dr,ddr starlike parametrization of the boundary
%        optional:
%          gew      weight of the quadratur formula (vector |i-j|)
%          sln      ln(4 sin^2((t_i-t_j)/2)         (vector |i-j|)
%          bessel1  J1(k|x-y|)
%          hankel1  Y1(k|x-y|)
%  OUTPUT: L        matrix
%          gew      weight                    (vector |i-j|)
%          sln      ln(4 sin^2((t_i-t_j)/2)   (vector |i-j|)
%          bessel1  J1(k|x-y|)
%          hankel1  Y1(k|x-y|)
%**************************************************************************

n = size(r,1) / 2;

% derivatives
if nargin < 3
   dr  = dtrig(r);
   ddr = dtrig(dr);
else
end

% preparing the quadratur formula
if nargin <5
   mintg = (1:n-1)';
   mcos = cos( pi/n * (0:2*n-1)' * mintg');
   gew = -2*pi/n*mcos*(ones(n-1,1)./mintg);
   gew = gew - pi/(n^2)*(-ones(2*n,1)).^((0:2*n-1)');

   sln = log(4*(sin( (1:2*n-1)' *pi / (2*n) )).^2);
   sln = [0;sln];

   Rij = toeplitz(gew);
   Sij = toeplitz(sln);
else
   Rij = toeplitz(gew);
   Sij = toeplitz(sln);
end

%  |x(t_i) - x(t_j)|
t     = pi/n * (0:2*n-1)';
tcos  = cos(t);
tsin  = sin(t);

x1    = r.*tcos;
x2    = r.*tsin;
dx1   = dr.*tcos - r.*tsin;
dx2   = dr.*tsin + r.*tcos;
nones = ones(size(r));
X1    = x1*nones';
X2    = x2*nones';
Diff  = sqrt((X1-X1').^2 + (X2-X2').^2);  

% bessel functions
if nargin < 7
   bessel1 = full(spfun('j1', triu(k*Diff,1)));
   hankel1 = full(spfun('y1', triu(k*Diff,1)));
   bessel1 = bessel1 + bessel1.';
   hankel1 = bessel1 + 1i*(hankel1 + hankel1.');
end

% matrix
Diff  = triu(Diff,1) + tril(Diff,-1) + diag(ones(size(r)));
absdx = sqrt(r.^2+dr.^2);
Tmp1  = (nones./absdx)*absdx';
L     = -1i*k/4*(hankel1)./Diff ;
Tmp2  = (X1-X1').*(dx2*nones') - (X2-X2').*(dx1 * nones');
L1    = k/4/pi*(bessel1./Diff).*Tmp2.*Tmp1;
L     = L.*Tmp2.*Tmp1;
tmp   = 1/4/pi * (r.*ddr - 2*dr.^2 - r.^2) ./ (absdx.^2); 
L     = L + diag(tmp); 
L2    = L - L1.*Sij;

L     = Rij.*L1 + pi/n * L2;


