# 2023-lf-fh Nonlinear Impedance Boundary Condition



## License

Copyright (c) 2023, Leonie Fink, Frank Hettlich

This software is released under GNU General Public License, Version 3.
The full license text is provided in the file LICENSE included in this repository
or can be obtained from http://www.gnu.org/licenses/

## Content of this project

This is a guide to generate the figures that have been used in the work

**"The domain derivative in inverse obstacle scattering with nonlinear impedance boundary condition"**

by Leonie Fink and Frank Hettlich.

The following versions of the paper are available:

- [ ] [CRC Preprint 2023/16, June 2023](https://www.waves.kit.edu/downloads/CRC1173_Preprint_2023-16.pdf)
- [ ] [Inverse Problems, 40: 015001, November 2023](https://iopscience.iop.org/article/10.1088/1361-6420/ad0c92)

You find all needed Matlab files to generate the figures.

## Requirements

The following additional software is necessary to run the code in this project:

- [ ] a recent version of Matlab

## An overview

- [ ] *arrow.m* creates an arrow indicating the incident direction of u^i
- [ ] *bndry.m* generates the boundary curve of the apple-shaped and peanut-shaped scattering obstacle, respectively.
- [ ] *dtrig.m* provides an approximation of the derivative of a 2pi periodic function by trigonometric interpolation.
- [ ] *noisec.m* adds p% random noise to the columns of a vector.
- [ ] *recipro.m* gives the reciprocal of the input.
- [ ] *select.m* selects 1/nth values from a vector.
- [ ] *j0.m* and *j1.m* are the Bessel functions of the first kind of orders 0 and 1, respectively.
- [ ] *y0.m* and *y1.m* are the Bessel functions of the second kind of orders 0 and 1, respectively.
- [ ] *imp.m* returns the impedance function \lambda with respect to the boundary parameterization, as well as its derivative.
- [ ] *g1.m* and *gu.m* indicate the nonlinear function, as well as its linearization.
- [ ] *inwave.m* returns the incident wave and its derivative on a starlike boundary.
- [ ] *SLP.m*, *DLP.m* and *NDSLP.m* are the matrices of the single layer potential, the double layer potential and the normal derivative of the single layer potential, respectively.
- [ ] *M_TM.m* contains the matrices for solving the boundary integral equations.
- [ ] *new_TM.m* contains the Newton method to solve the inverse problem.
- [ ] *plot_inverse.m* and *plot_invstat.m*  provide the representation of the plots generated by *inverse.m* and *invstat.m* respectively.
- [ ] *inverse.m* tests the iterative Newton method for the inverse problem with noise-free synthetic data.
- [ ] *invstat.m* tests the iterative Newton method for the inverse problem, adding 5% equally distributed random noise to the synthetic data.

## Generating the files

For generating the figures 1 - 3 from the work, run

* *inverse.m* - **Fig 1** (Example 1, apple-shape, 0% noise)
* *invstat.m* -  **Fig 2** (Example 1, peanut-shape, 5% noise, no changes in code)
* *invstat.m* -  **Fig 3** (Example 2, garlic-shape, 5% noise, change the functions g and gz and all parameters with the comment 'Example 2')
* *invstat.m*               (Example 3, galic-shape, 5% noise, change the functions g and gz and all parameters with the comment 'Example 3')
